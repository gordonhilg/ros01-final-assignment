/*
 * airbag_task.h
 *
 *  Created on: 12 jan. 2015
 *      Author: Gordon & Remi
 */

#ifndef AIRBAG_TASK_H_
#define AIRBAG_TASK_H_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "drivers/rgb.h"
#include "drivers/buttons.h"
#include "utils/uartstdio.h"
#include "priorities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "defines.h"

//*****************************************************************************
//
// global variables
//
//*****************************************************************************
extern portTickType ui32IOIntWakeTime;
xTaskHandle xAirbagHandle;


//*****************************************************************************
//
// Semaphores used to get control of a peripheral
//
//*****************************************************************************
extern xSemaphoreHandle g_pUARTSemaphore;
extern xSemaphoreHandle g_pAirbagIntSemaphore;

//*****************************************************************************
//
// Defines used in the airbag task
//
//*****************************************************************************
#define AIRBAG_TASK_STACK_SIZE                  128         // Stack size in words

#define QUEUE_SIZE                              5
#define QUEUE_ITEM_SIZE                         sizeof(uint32_t)

#define AIRBAG_TASK_DELAY_MS                    32
#define AIRBAG_TASK_MAX_DELAY                   50


//*****************************************************************************
//
// Prototypes for the Airconditioning task.
//
//*****************************************************************************
extern uint32_t airbagTaskInit(void);


#endif /* AIRBAG_TASK_H_ */
