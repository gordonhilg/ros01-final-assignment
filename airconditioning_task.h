/*
 * airconditioning_task.h
 *
 *  Created on: 12 jan. 2015
 *      Author: Gordon & Remi
 */

#ifndef AIRCONDITIONING_TASK_H_
#define AIRCONDITIONING_TASK_H_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "drivers/rgb.h"
#include "drivers/buttons.h"
#include "utils/uartstdio.h"
#include "priorities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"


//*****************************************************************************
//
// global variables
//
//*****************************************************************************
xTaskHandle xAircoHandle;                       //task handle, used to resume task from ISR
extern portTickType ui32IOIntWakeTime;

//*****************************************************************************
//
// Semaphores
//
//*****************************************************************************
extern xSemaphoreHandle g_pUARTSemaphore;
extern xSemaphoreHandle g_pAircoIntSemaphore;


//*****************************************************************************
//
// defines needed by airco task
//
//*****************************************************************************
#define AIRCO_TASK_STACK_SIZE                   128
#define AIRCO_TASK_DELAY_MS                     22
#define AIRCO_TASK_MAX_DELAY                    40

//*****************************************************************************
//
// Prototypes for the Airconditioning task.
//
//*****************************************************************************
extern uint32_t AircoTaskInit(void);


#endif /* AIRCONDITIONING_TASK_H_ */
