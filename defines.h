/*
 * defines.h
 *
 *  Created on: 24 jan. 2015
 *      Author: Gordon
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#include "driverlib/gpio.h"

#define ALL_GPIO_LED_PINS       RED_GPIO_PIN | BLUE_GPIO_PIN | GREEN_GPIO_PIN


#endif /* DEFINES_H_ */
