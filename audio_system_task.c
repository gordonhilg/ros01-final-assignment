/*
 * audio_system_task.c
 *
 *  Created on: 12 jan. 2015
 *      Author: Gordon & Remi
 */
#include "audio_system_task.h"

static void
audioTask(void *pvParameters)
{
    uint32_t ui32ADCValue;
    portTickType ui32AudioIntTime;
    portTickType ui32AudioDelayTime;
    uint32_t ui32UARTValue;

    while(1)
   {
       //suspending task till wakeup from ISR
       vTaskSuspend(xAudioHandle);

       //save the time at which this function was resumed, for timing information
       ui32AudioIntTime = ui32IOIntWakeTime;

       //execute when semaphore is given from ISR
       if(xSemaphoreTake(g_pAudioIntSemaphore, portMAX_DELAY))
       {
           //delay task for some time to keep response between 50 - 80ms
           vTaskDelay(AUDIO_TASK_DELAY_MS/portTICK_RATE_MS);

           //turn the led on
           if(xSemaphoreTake(g_pLEDSemaphore, portMAX_DELAY) == pdTRUE)
           {
               GPIOPinWrite(GPIO_PORTF_BASE, GREEN_GPIO_PIN | BLUE_GPIO_PIN, GREEN_GPIO_PIN);

               ui32AudioDelayTime = xTaskGetTickCount();

               //get ADC value
               ui32ADCValue = getADCValue();

               //send ADC value over UART
               ui32UARTValue = 0xC5431000 | (ui32ADCValue & 0x00000FFF);

               if(xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY) == pdTRUE)
               {
                   UARTprintf("%X\n", ui32UARTValue);
                   xSemaphoreGive(g_pUARTSemaphore);
               }

               //set pin PA5 HIGH
               GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_5, GPIO_PIN_5);

               //wait for 1 ms
               vTaskDelay(1/portTICK_RATE_MS);

               //set pin PA5 LOW
               GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_5, 0x00);

               //Send message to arduino about finishing the task
               if(xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY) == pdTRUE)
               {
                   if((xTaskGetTickCount() - ui32AudioIntTime) < AUDIO_TASK_MAX_DELAY)
                   {
                       UARTprintf("CC.SS %u\n", xTaskGetTickCount() - ui32AudioIntTime);
                   }
                   else
                   {
                       UARTprintf("CC.F %u\n", xTaskGetTickCount() - ui32AudioIntTime);
                   }

                   xSemaphoreGive(g_pUARTSemaphore);
               }

#if (configUSE_UART == 1)
          xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY);
          UARTprintf("Hello from the audio task!\n");
          UARTprintf("Interrupt at: %u\n", ui32AudioIntTime);
          UARTprintf("Delayed till: %u\n", ui32AudioDelayTime);
          UARTprintf("Finished at: %u\n", xTaskGetTickCount());
          UARTprintf("Ticks between: %u \n", (ui32AudioDelayTime - ui32AudioIntTime));
          xSemaphoreGive(g_pUARTSemaphore);
#endif
               //turn the led back off
               GPIOPinWrite(GPIO_PORTF_BASE, GREEN_GPIO_PIN | BLUE_GPIO_PIN, 0x00);
               xSemaphoreGive(g_pLEDSemaphore);
            }
        }
    }
}

uint32_t getADCValue(void)
{
    // Set the variable for ADC registers
    uint32_t pui32ADC0Value[8];

    //
    // Trigger the ADC conversion.
    //
    ADCProcessorTrigger(ADC0_BASE, 2);

    //
    // Wait for conversion to be completed.
    //
    while(!ADCIntStatus(ADC0_BASE, 2, false))
    {
    }

    //
    // Clear the ADC interrupt flag.
    //
    ADCIntClear(ADC0_BASE, 2);

    //
    // Read ADC Value.
    //
    ADCSequenceDataGet(ADC0_BASE, 2, pui32ADC0Value);

    uint32_t avgValue = (pui32ADC0Value[0] + pui32ADC0Value[1] + pui32ADC0Value[2]+ pui32ADC0Value[3]) / 4;

    return avgValue;
}


uint32_t audioTaskInit(void)
{
#if (configUSE_UART == 1)
    xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY);
    UARTprintf("Initializing audio task.\n");
    xSemaphoreGive(g_pUARTSemaphore);
#endif
    //
    // Create the airbag task
    //
    if(xTaskCreate(audioTask, (signed portCHAR *)"AUDIO", AUDIO_TASK_STACK_SIZE, NULL,
                   tskIDLE_PRIORITY + PRIORITY_AUDIO_TASK, &xAudioHandle) != pdTRUE)
    {
        return(1);
    }
    //
    // Success.
    //
    return(0);
}
