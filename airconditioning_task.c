/*
 * airconditioning_task.c
 *
 *  Created on: 12 jan. 2015
 *      Author: Gordon & Remi
 */

#include "airconditioning_task.h"

static void
AircoTask(void *pvParameters)
{
    uint32_t ui32UARTValue = 0x854325A1;
    portTickType ui32AircoIntTime;
    portTickType ui32AircoDelayTime;

    while(1)
    {
        //suspending task till wakeup from ISR
        vTaskSuspend(xAircoHandle);

        //save the time at which this function was resumed, for timing information
        ui32AircoIntTime = ui32IOIntWakeTime;

        //execute when semaphore is given from ISR
        if(xSemaphoreTake(g_pAircoIntSemaphore, portMAX_DELAY))
        {
            //delay task for some time to keep response between 50 - 80ms
            vTaskDelay(AIRCO_TASK_DELAY_MS/portTICK_RATE_MS);
            ui32AircoDelayTime = xTaskGetTickCount();


            //send predefined register over UART
            if(xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY) == pdTRUE)
            {
                UARTprintf("%X\n", ui32UARTValue);
                xSemaphoreGive(g_pUARTSemaphore);
            }

            //turn pin PA7 on
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_PIN_7);

            //wait for 1 ms
            vTaskDelay(1/portTICK_RATE_MS);

            //set pin PA5 LOW
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, 0x00);

            //Send message to arduino about finishing the task
           if(xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY) == pdTRUE)
           {
               if((xTaskGetTickCount() - ui32AircoIntTime) < AIRCO_TASK_MAX_DELAY)
               {
                   UARTprintf("AC.SS %u\n", xTaskGetTickCount() - ui32AircoIntTime);
               }
               else
               {
                   UARTprintf("AC.F %u\n", xTaskGetTickCount() - ui32AircoIntTime);
               }

               xSemaphoreGive(g_pUARTSemaphore);
           }

#if (configUSE_UART == 1)
           xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY);
           UARTprintf("Hello from the airco task!\n");
           UARTprintf("Interrupt at: %u\n", ui32AircoIntTime);
           UARTprintf("Delayed till: %u\n", ui32AircoDelayTime);
           UARTprintf("Finished at: %u\n", xTaskGetTickCount());
           UARTprintf("Ticks between: %u \n", (ui32AircoDelayTime - ui32AircoIntTime));
           xSemaphoreGive(g_pUARTSemaphore);
#endif

        }
    }

}

uint32_t AircoTaskInit(void)
{
#if (configUSE_UART == 1)
    xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY);
    UARTprintf("Initializing airco task.\n");
    xSemaphoreGive(g_pUARTSemaphore);
#endif
    //
    // Create the airbag task
    //
    if(xTaskCreate(AircoTask, (signed portCHAR *)"AIRCO", AIRCO_TASK_STACK_SIZE, NULL,
                   tskIDLE_PRIORITY + PRIORITY_AIRCO_TASK, &xAircoHandle) != pdTRUE)
    {
        return(1);
    }
    //
    // Success.
    //
    return(0);
}
