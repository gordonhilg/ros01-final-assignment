/*
 * audio_system_task.h
 *
 *  Created on: 12 jan. 2015
 *      Author: Gordon & Remi
 */

#ifndef AUDIO_SYSTEM_TASK_H_
#define AUDIO_SYSTEM_TASK_H_
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "drivers/rgb.h"
#include "drivers/buttons.h"
#include "utils/uartstdio.h"
#include "priorities.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"


//*****************************************************************************
//
// global variables
//
//*****************************************************************************
xTaskHandle xAudioHandle;                       //task handle, used to resume task from ISR
extern portTickType ui32IOIntWakeTime;


//*****************************************************************************
//
// Semaphores
//
//*****************************************************************************
extern xSemaphoreHandle g_pUARTSemaphore;
extern xSemaphoreHandle g_pAudioIntSemaphore;
extern xSemaphoreHandle g_pLEDSemaphore;


//*****************************************************************************
//
// defines needed by audio task
//
//*****************************************************************************
#define AUDIO_TASK_STACK_SIZE               128         // Stack size in words

#define AUDIO_TASK_DELAY_MS                 52
#define AUDIO_TASK_MAX_DELAY                80


//*****************************************************************************
//
// Prototypes for the Airconditioning task.
//
//*****************************************************************************
uint32_t getADCValue(void);

extern uint32_t audioTaskInit(void);


#endif /* AUDIO_SYSTEM_TASK_H_ */
