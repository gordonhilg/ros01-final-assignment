/*
 * airbag_task.c
 *
 *  Created on: 12 jan. 2015
 *      Author: Gordon & Remi
 */
#include "airbag_task.h"

static void airbagTask(void *pvParameters)
{
    portTickType ui32AirbagIntTime;
    portTickType ui32AirbagDelayTime;

    while(1)
    {
        //suspending task till wakeup from ISR
        vTaskSuspend(xAirbagHandle);

        //save the time at which this function was resumed, for timing information
        ui32AirbagIntTime = ui32IOIntWakeTime;

        //execute when semaphore is given from ISR
        if(xSemaphoreTake(g_pAirbagIntSemaphore, portMAX_DELAY))
        {

            //delay task for some time to keep response between 30 - 50ms
            vTaskDelay(AIRBAG_TASK_DELAY_MS/portTICK_RATE_MS);
            ui32AirbagDelayTime = xTaskGetTickCount();

            //set pin PA6 HIGH
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6, GPIO_PIN_6);

            //keep pin high for 1ms
            vTaskDelay(1/portTICK_RATE_MS);

            //set pin PA6 LOW
            GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6, 0x00);

            //Send message to arduino about finishing the task
            if(xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY) == pdTRUE)
            {
                if((xTaskGetTickCount() - ui32AirbagIntTime) < AIRBAG_TASK_MAX_DELAY)
                {
                    UARTprintf("AB.SS %u\n", xTaskGetTickCount() - ui32AirbagIntTime);
                }
                else
                {
                    UARTprintf("AB.F %u\n", xTaskGetTickCount() - ui32AirbagIntTime);
                }

                xSemaphoreGive(g_pUARTSemaphore);
            }

#if (configUSE_UART == 1)
           xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY);
           UARTprintf("Hello from the airco task!\n");
           UARTprintf("Interrupt at: %u\n", ui32AirbagIntTime);
           UARTprintf("Delayed till: %u\n", ui32AirbagDelayTime);
           UARTprintf("Finished at: %u\n", xTaskGetTickCount());
           UARTprintf("Ticks between: %u \n", (ui32AirbagDelayTime - ui32AirbagIntTime));
           xSemaphoreGive(g_pUARTSemaphore);
#endif
        }
    }
}

uint32_t airbagTaskInit(void)
{
#if (configUSE_UART == 1)
    xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY);
    UARTprintf("Initializing airbag task.\n");
    xSemaphoreGive(g_pUARTSemaphore);
#endif
    //
    // Create the airbag task
    //
    if(xTaskCreate(airbagTask, (signed portCHAR *)"AIRBAG", AIRBAG_TASK_STACK_SIZE, NULL,
                   tskIDLE_PRIORITY + PRIORITY_AIRBAG_TASK, &xAirbagHandle) != pdTRUE)
    {
        return(1);
    }
    //
    // Success.
    //
    return(0);
}

