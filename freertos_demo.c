//*****************************************************************************
//
// freertos_demo.c - Simple FreeRTOS example.
//
// Copyright (c) 2012-2014 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
// 
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
// 
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
// 
// This is part of revision 2.1.0.12573 of the EK-TM4C123GXL Firmware Package.
//
// Modified by Gordon & Remi for ROS01 Final Assignment
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/interrupt.h"
#include "utils/uartstdio.h"
#include "led_task.h"
#include "FreeRTOS.h"
#include "airbag_task.h"
#include "airconditioning_task.h"
#include "audio_system_task.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "defines.h"

//*****************************************************************************
//
// The mutex that protects concurrent access of UART from multiple tasks.
//
//*****************************************************************************
xSemaphoreHandle g_pUARTSemaphore;
xSemaphoreHandle g_pLEDSemaphore;
xSemaphoreHandle g_pAirbagIntSemaphore;
xSemaphoreHandle g_pAudioIntSemaphore;
xSemaphoreHandle g_pAircoIntSemaphore;

extern xTaskHandle xAirbagHandle;
extern xTaskHandle xAudioHandle;
extern xTaskHandle xAircoHandle;

portTickType ui32IOIntWakeTime;


//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}

#endif

//*****************************************************************************
//
// This hook is called by FreeRTOS when an stack overflow error is detected.
//
//*****************************************************************************
void
vApplicationStackOverflowHook(xTaskHandle *pxTask, char *pcTaskName)
{
    //
    // This function can not return, so loop forever.  Interrupts are disabled
    // on entry to this function, so no processor interrupts will interrupt
    // this loop.
    //
    while(1)
    {
    }
}

//*****************************************************************************
//
// interrupt routine that gets called at a pin interrupt
//
//*****************************************************************************
void IOIntHandler(void)
{
    signed long sHigherPriorityTaskWoken = pdFALSE;
    signed long sHigherPriorityTaskWokenTemp = pdFALSE;
    //get current tick counter
    ui32IOIntWakeTime = xTaskGetTickCountFromISR();
    uint8_t PB4 = GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_4);
    uint8_t PE4 = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_4);
    uint8_t PE5 = GPIOPinRead(GPIO_PORTE_BASE, GPIO_PIN_5);
    uint8_t PF4 = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4);

#if (configUSE_UART == 1)
    UARTprintf("Hello from interrupt routine!\n");
    UARTprintf("PB4: %u\n",PB4);
    UARTprintf("PE4: %u\n",PE4);
    UARTprintf("PE5: %u\n",PE5);
    UARTprintf("PF4: %u\n",PF4);
#endif
    //start airbag task
    if(PE5)
    {
        //make sure the semaphore is given to the right task and handles the interrupt
        xSemaphoreGiveFromISR(g_pAirbagIntSemaphore, &sHigherPriorityTaskWokenTemp);
        sHigherPriorityTaskWoken |= sHigherPriorityTaskWokenTemp;
        xTaskResumeFromISR(xAirbagHandle);
    }
    //start airco task
    if(PE4)
    {
        xSemaphoreGiveFromISR(g_pAircoIntSemaphore, &sHigherPriorityTaskWokenTemp);

        sHigherPriorityTaskWoken |= sHigherPriorityTaskWokenTemp;
        xTaskResumeFromISR(xAircoHandle);
    }
    //start Audio event
    if(PB4)
    {
        xSemaphoreGiveFromISR(g_pAudioIntSemaphore, &sHigherPriorityTaskWokenTemp);

        sHigherPriorityTaskWoken |= sHigherPriorityTaskWokenTemp;
        xTaskResumeFromISR(xAudioHandle);
    }
    GPIOIntClear(GPIO_PORTE_BASE, GPIO_PIN_5);
    GPIOIntClear(GPIO_PORTE_BASE, GPIO_PIN_4);
    GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);
    GPIOIntClear(GPIO_PORTB_BASE, GPIO_PIN_4);

    portYIELD_FROM_ISR(sHigherPriorityTaskWoken);
}


//*****************************************************************************
//
// Configure the UART and its pins.  This must be called before UARTprintf().
//
//*****************************************************************************
void
ConfigureUART(void)
{
    //
    // Enable the GPIO Peripheral used by the UART.
    //
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    //
    // Enable UART0
    //
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);

    //
    // Configure GPIO Pins for UART mode.
    //
    ROM_GPIOPinConfigure(GPIO_PB0_U1RX);
    ROM_GPIOPinConfigure(GPIO_PB1_U1TX);
    ROM_GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    //
    // Use the internal 16MHz oscillator as the UART clock source.
    //
    UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);

    //
    // Initialize the UART for console I/O.
    //
    UARTStdioConfig(0, 9600, 16000000);
}

void configureInterrupts()
{
#if (configUSE_UART == 1)
    UARTprintf("Configure Interrupts\n");
#endif
    //enable interrupts op PORT B and E
    IntMasterEnable();
    IntEnable(17);      //port B
    IntEnable(20);      //port E
    IntEnable(46);      //port F

    //setup interrupts on PB4, rising edge
    GPIOIntDisable(GPIO_PORTB_BASE, GPIO_PIN_4);
    GPIOIntClear(GPIO_PORTB_BASE, GPIO_PIN_4);
    GPIOIntRegister(GPIO_PORTB_BASE, IOIntHandler);
    GPIOIntTypeSet(GPIO_PORTB_BASE, GPIO_PIN_4, GPIO_RISING_EDGE);
    GPIOIntEnable(GPIO_PORTB_BASE, GPIO_PIN_4);

    //setup interrupts on PE4, 5, rising edge
    GPIOIntDisable(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);
    GPIOIntClear(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);
    GPIOIntRegister(GPIO_PORTE_BASE, IOIntHandler);
    GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5, GPIO_RISING_EDGE);
    GPIOIntEnable(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);

    //setup interrupts on PF4 rising edge
    GPIOIntDisable(GPIO_PORTF_BASE, GPIO_PIN_4);
    GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);
    GPIOIntRegister(GPIO_PORTF_BASE, IOIntHandler);
    GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_FALLING_EDGE);
    GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);
}

void configureGPIO()
{
#if (configUSE_UART == 1)
    UARTprintf("Configure GPIO\n");
#endif
    //configure PA5,6,7 as output
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE,  GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

    //configure PB4 as input
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_4);
    GPIOPadConfigSet(GPIO_PORTB_BASE, GPIO_PIN_4, GPIO_STRENGTH_4MA,
           GPIO_PIN_TYPE_STD);
    //configure PE4,5 as input
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5);
    GPIOPadConfigSet(GPIO_PORTE_BASE, GPIO_PIN_4 | GPIO_PIN_5, GPIO_STRENGTH_4MA,
           GPIO_PIN_TYPE_STD);

    //configure PF4 as input
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_4MA,
           GPIO_PIN_TYPE_STD_WPU);

    //enable the leds and turn them off
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, ALL_GPIO_LED_PINS);
    GPIOPinWrite(GPIO_PORTF_BASE, ALL_GPIO_LED_PINS, 0x00);
}

void configureADC(void)
{
    // The ADC0 peripheral must be enabled for use.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);


    // Enable sample sequence 2 with a processor signal trigger
    ADCSequenceConfigure(ADC0_BASE, 2, ADC_TRIGGER_PROCESSOR, 0);

    // Configure step 0 through 4 for sequence 2
    // Enable trigger and sequence and select on last step
    ADCSequenceStepConfigure(ADC0_BASE, 2, 0, ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 1, ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 2, ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 3, ADC_CTL_CH1 | ADC_CTL_IE |ADC_CTL_END);

    // Enable sample sequence 2
    ADCSequenceEnable(ADC0_BASE, 2);

    // Clear the interrupt status flag for sequence 2
    ADCIntClear(ADC0_BASE, 2);
}

//*****************************************************************************
//
// Initialize FreeRTOS and start the initial set of tasks.
//
//*****************************************************************************
int main(void)
{
    //
    // Set the clocking to run at 50 MHz from the PLL.
    //
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);

    //
    // Initialize the UART and configure it for 115.200, 8-N-1 operation.
    //
    ConfigureUART();

#if (configUSE_UART == 1)
    //
    // Print introduction.
    //
    UARTprintf("\033[?25l");
    UARTprintf("\033[1J");
    UARTprintf("\033[0m \033[0m \033[1;1H");
    UARTprintf("Welcome to the ROS01 Final assignment by Gordon Hilgers - 0838866 & Remi Schipperus - 0837748\n\n");
    UARTprintf("Information:\n");
    //UARTprintf("100/portTICK_RATE_MS: %u\n", 100/portTICK_RATE_MS);
#endif
    //
    // Create a mutex to guard the UART and LED.
    //
    g_pUARTSemaphore = xSemaphoreCreateMutex();

    g_pLEDSemaphore = xSemaphoreCreateMutex();

    vSemaphoreCreateBinary(g_pAirbagIntSemaphore);
    vSemaphoreCreateBinary(g_pAudioIntSemaphore);
    vSemaphoreCreateBinary(g_pAircoIntSemaphore);


    configureGPIO();
    configureInterrupts();
    configureADC();
    //
    //create the Airbag task
    //
    if(airbagTaskInit() != 0)
    {
        while(1)
        {
        }
    }

    //
    //create the audio task
    //
    if(audioTaskInit() != 0)
    {
        while(1)
        {
        }
    }

    if(AircoTaskInit() != 0)
    {
        while(1)
        {
        }
    }

    //
    // Create the LED task.
    //
    if(LEDTaskInit() != 0)
    {
        while(1)
        {
        }
    }

#if (configUSE_UART == 1)
    xSemaphoreTake(g_pUARTSemaphore, portMAX_DELAY);
    UARTprintf("Finished initializing the tasks, starting scheduler now.\n");
    xSemaphoreGive(g_pUARTSemaphore);
#endif
    //
    // Start the scheduler.  This should not return.
    //
    vTaskStartScheduler();

    //
    // In case the scheduler returns for some reason, print an error and loop
    // forever.
    //

    while(1)
    {
    }
}
